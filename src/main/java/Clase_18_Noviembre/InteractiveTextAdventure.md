# Interactive Text Adventure

Una aventura de texto interactiva es un tipo de juego en el que los jugadores toman decisiones a lo largo de una historia presentada en texto. Este tipo de juego puede ser muy divertido y educativo para practicar la lógica de programación y los conceptos de OOP en Java.

### Estructura básica de una aventura de texto:

1. **Historia y Escenarios:** La aventura se desarrolla a través de una serie de escenarios o situaciones presentadas en texto. Estos escenarios pueden ser habitaciones, lugares, eventos, etc. Cada escenario tiene una descripción que se muestra al jugador.

2. **Toma de Decisiones:** Después de leer la descripción de un escenario, el jugador toma decisiones escribiendo comandos en la consola, por ejemplo, "ir hacia la puerta" o "recoger el objeto".

3. **Interacción con Objetos y Personajes:** El jugador puede interactuar con objetos, personajes u otros elementos en el juego. Estas interacciones pueden afectar la historia o llevar a diferentes escenarios.

### Implementación en Java:

En la implementación de una aventura de texto en Java, podrías utilizar clases para representar diferentes elementos del juego:

- **Clase Escenario:** Puede contener la descripción del lugar o situación actual y métodos para manejar las acciones del jugador en ese escenario.
- **Clase Jugador:** Representa al jugador con atributos como inventario, estado, etc. Puede tener métodos para moverse entre escenarios, interactuar con objetos, tomar decisiones, etc.
- **Clase Objeto:** Para representar los objetos en el juego, como llaves, libros, pociones, etc.
- **Clase Personaje:** Si hay personajes con los que el jugador puede interactuar, como un guía, un enemigo, etc.

### Ejemplo básico:

```java
public class Escenario {
    private String descripcion;
    private Escenario[] opciones;

    public Escenario(String descripcion, Escenario[] opciones) {
        this.descripcion = descripcion;
        this.opciones = opciones;
    }

    public void mostrarDescripcion() {
        System.out.println(descripcion);
    }

    public Escenario[] getOpciones() {
        return opciones;
    }
}

// Lógica para manejar la interacción del jugador con los escenarios, objetos, etc.
// Puedes implementar métodos para que el jugador avance en la historia, tome decisiones, etc.
```

Esta es solo una estructura básica para empezar. La clave es diseñar la lógica del juego con una estructura de clases que permita la interacción del jugador con diferentes escenarios y elementos para crear una experiencia de juego interactiva y emocionante.

## Historia

¡Claro! Ampliemos la historia para hacerla más completa y emocionante:

---

### "La Mansión del Tesoro Olvidado"

#### Descripción inicial:
Te encuentras frente a una enorme mansión envuelta en niebla. Su estructura gótica y las enredaderas cubriendo las paredes te llenan de intriga. Has oído rumores sobre un tesoro perdido que se esconde dentro de estas paredes centenarias. Decides aventurarte adentro en busca de la riqueza olvidada.

#### Escenario 1 - Salón Principal:
Al entrar, te encuentras en un salón inmenso con muebles cubiertos por sábanas. A lo lejos, una escalera de caracol conduce a los niveles superiores. El brillo de algo metálico se vislumbra desde la chimenea.

#### Escenario 2 - Biblioteca:
Subes por la escalera y te encuentras en una biblioteca polvorienta. Estanterías llenas de libros antiguos rodean la habitación. Una extraña inscripción en latín en una pared llama tu atención: "Audi, Vide, Tace".

#### Escenario 3 - Habitación del Tesoro:
Resolviendo el enigma, una puerta secreta se abre revelando una habitación oculta. Un cofre de madera antigua descansa en el centro. Parece estar cerrado con un candado.

#### Escenario 4 - Jardines Misteriosos:
Exploras los jardines traseros y descubres un laberinto de arbustos y estatuas. El sonido del agua corriendo te guía hacia una fuente en la que destella un anillo dorado.

#### Escenario 5 - Salón de Retratos:
Una serie de retratos antiguos adorna las paredes de este salón. Uno de los retratos parece tener los ojos que siguen tus movimientos.

#### Escenario 6 - Sótano Oscuro:
Bajando unas escaleras empinadas, llegas al oscuro sótano. Un ruido misterioso te hace temblar. ¿Será el viento o algo más?

---

Estos son algunos ejemplos adicionales de escenarios que podrían incluirse en la aventura. Puedes expandir la historia agregando más escenarios, desafíos, pistas o interacciones con objetos y personajes para crear una experiencia más inmersiva y desafiante para el jugador.