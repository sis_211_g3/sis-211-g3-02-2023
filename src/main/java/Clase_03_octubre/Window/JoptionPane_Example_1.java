/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Clase_03_octubre.Window;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author PERSONAL
 */
public class JoptionPane_Example_1 {

    // un regitro de estudiantes
    public static void main(String[] args) {
        // opcion 1
        // Estudiante[] estudiantes = new Estudiante[1];

        // opcion 2
        // lista -> [1]
        //          [2]-> 4 -> 8 -> 16
        List<Estudiante> estudiantes = new ArrayList<>();
        int result2;
        do {
            // creacion de los campos de texto
            JTextField name = new JTextField(5);
            JTextField lastName = new JTextField(5);
            JTextField age = new JTextField(5);

            // creacion del panel
            JPanel panel = new JPanel();
            panel.add(new JLabel("Nombre:"));
            panel.add(name);
            panel.add(new JLabel("Apellido:"));
            panel.add(lastName);
            panel.add(new JLabel("Edad:"));
            panel.add(age);
            int result = JOptionPane.showConfirmDialog(
                    null,
                    panel,
                    "Introdusca los datos del estudiante",
                    JOptionPane.OK_OPTION);
            if (result == 0) {
                // creamo un objeto de la clase estudiante
                Estudiante estudiante = new Estudiante(name.getText(), lastName.getText(), Integer.parseInt(age.getText()));
                // gusrdamos al estudiante en lam lsiat de estudiantes
                estudiantes.add(estudiante);
                System.out.println(estudiante);
            } else {
                System.out.println("No se regitraron datos");
            }
            System.out.println(estudiantes);
            System.out.println("============================");
            result2 = JOptionPane.showConfirmDialog(null, "Quiere seguir regitrando estudiantes", null, JOptionPane.OK_OPTION, JOptionPane.CANCEL_OPTION);
        } while (result2 != JOptionPane.NO_OPTION);
        System.out.println("fin del programa");
    }

    public static Estudiante[] extendLimit(Estudiante[] estudiantes) {
        return Arrays.copyOf(estudiantes, estudiantes.length + 1);
    }
}
