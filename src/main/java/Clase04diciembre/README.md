Crea un sistema en Java para modelar las tropas de Clash of Clans. Utiliza la herencia y el polimorfismo para representar distintos tipos de tropas.

1. Define una clase base abstracta `Tropa` que tenga los siguientes atributos:
    - `nombre` (String): el nombre de la tropa.
    - `nivel` (int): el nivel de la tropa.
    - `tipo` (String): el tipo de la tropa (por ejemplo, "Terrestre" o "Aérea").

   Esta clase debe incluir un método abstracto `atacar()` que será implementado por las clases hijas.

2. Crea al menos tres clases que hereden de `Tropa`, por ejemplo: `TropaTerrestre`, `TropaAerea` y `TropaEspecial`. Cada una debe implementar el método `atacar()` de acuerdo a las características de esa tropa.

3. Agrega características específicas para cada tipo de tropa. Por ejemplo:
    - `TropaTerrestre` puede tener un atributo `velocidad`.
    - `TropaAerea` puede tener un atributo `alturaDeVuelo`.
    - `TropaEspecial` puede tener un atributo `habilidadEspecial`.

4. Crea una clase principal `SimulacionBatalla` donde puedas instanciar diferentes tipos de tropas y simular una batalla entre ellas. Puedes crear un método que simule una serie de ataques entre diferentes tropas.

5. Utiliza la herencia para evitar duplicar código y aprovecha el polimorfismo para invocar el método `atacar()` de las tropas de manera genérica en la simulación de la batalla.

6. Experimenta con diferentes combinaciones de tropas, sus habilidades y niveles para observar cómo se desarrollan las batallas simuladas.

Recuerda que este es un ejercicio básico para comprender la herencia y el polimorfismo en Java. Puedes expandirlo aún más agregando más tipos de tropas, habilidades especiales, estrategias de ataque, etc.

---

Esta expansión te permite crear un sistema más complejo donde diferentes tipos de tropas con habilidades únicas pueden enfrentarse en una simulación de batalla. Puedes continuar ampliando el proyecto agregando más funcionalidades y características según tu imaginación y los conceptos que desees explorar.