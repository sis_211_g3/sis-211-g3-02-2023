package Clase04diciembre.Models;

public abstract class Tropa {
    private String nombre;
    private Integer nivel;
    private String tipo;

    private Integer danio;

    private Integer vida;

    private Integer tiempoCarga;


    public Integer getVida() {
        return vida;
    }

    public void setVida(Integer vida) {
        this.vida = vida;
    }

    public Tropa(String nombre,Integer danio, Integer nivel, String tipo, Integer vida) {
        this.nombre = nombre;
        this.nivel = nivel;
        this.tipo = tipo;
        this.vida = vida;
        this.danio = danio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getNivel() {
        return nivel;
    }

    public void setNivel(Integer nivel) {
        this.nivel = nivel;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public abstract void atacar(Tropa defensa, Tropa atacante);
    Runnable runnable = new Runnable() {
        @Override
        public void run() {

        }
    };
    public Integer getDanio() {
        return danio;
    }

    public void setDanio(Integer danio) {
        this.danio = danio;
    }

    @Override
    public String toString() {
        return "Tropa{" +
                "nombre='" + nombre + '\'' +
                ", nivel=" + nivel +
                ", tipo='" + tipo + '\'' +
                ", danio=" + danio +
                ", vida=" + vida +
                '}';
    }
}
