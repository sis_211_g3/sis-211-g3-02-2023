package Clase04diciembre;

import Clase04diciembre.Clases.TropaAerea;
import Clase04diciembre.Clases.TropaTerrestre;

import java.sql.Array;
import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        TropaTerrestre mago = new TropaTerrestre(
                "mago",
                100,
                2,
                "terrestre",
                4000,
                10);
        System.out.println(mago.getNombre());
        TropaTerrestre barbaro = new TropaTerrestre(
                "barbaros",
                50,
                2,
                "terrestre",
                2000,
                20);
        TropaAerea dragonElectrico = new TropaAerea(
                "Dragon Electrico",
                330,
                4,
                "Aereo",
                4500,
                20
        );
        System.out.println(mago);
        System.out.println(barbaro);
        System.out.println(dragonElectrico);
        /*System.out.println("Antes");
        System.out.println(mago);
        System.out.println(barbaro);
        mago.atacar(barbaro);
        System.out.println("Despues");
        System.out.println(mago);
        System.out.println(barbaro);*/

        /*System.out.println("Nuevo ataque mago vs Dragon Electrico");
        System.out.println("Antes");
        System.out.println(mago);
        System.out.println(dragonElectrico);
        mago.atacar(dragonElectrico);
        System.out.println("Despues");
        System.out.println(mago);
        System.out.println(dragonElectrico);

        System.out.println("Segundo ataque");
        System.out.println("Antes");
        System.out.println(mago);
        System.out.println(dragonElectrico);
        mago.atacar(dragonElectrico);
        System.out.println("Despues");
        System.out.println(mago);
        System.out.println(dragonElectrico);*/

        /*while (true){
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println("Tiempo");
        }*/
        ArrayList<Integer> integers = new ArrayList<>();
        integers.add(50);
        integers.add(10);
        integers.add(40);
        integers.add(100);

        integers.forEach(System.out::println);
    }
}
