package Clase04diciembre.Clases;

import Clase04diciembre.Models.Tropa;

public class TropaEspecial extends Tropa {
    private String habilidadEspecial;
    public TropaEspecial(String nombre,
                         Integer danio,
                         Integer nivel,
                         String tipo,
                         Integer vida,
                         String habilidadEspecial) {
        super(nombre, danio, nivel, tipo, vida);
        this.habilidadEspecial = habilidadEspecial;
    }

    @Override
    public void atacar(Tropa defensa, Tropa atacante) {
        while (defensa.getVida() > 0 && atacante.getVida() > 0){

        }
    }
}
