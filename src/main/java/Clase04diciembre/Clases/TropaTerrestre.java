package Clase04diciembre.Clases;

import Clase04diciembre.Models.Tropa;

public class TropaTerrestre extends Tropa {
    private Integer velocidad;

    public TropaTerrestre(String nombre,
                          Integer danio,
                          Integer nivel,
                          String tipo,
                          Integer vida,
                          Integer velocidad){
        super(nombre, danio, nivel, tipo, vida);
        this.velocidad = velocidad;
    }

    @Override
    public void atacar(Tropa defensa, Tropa atacante) {
        while (defensa.getVida() > 0 && atacante.getVida() > 0){

        }
    }
}
