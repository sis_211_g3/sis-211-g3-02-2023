package Clase27noviembre.GestionDeTareas;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

public class Tarea {
    private String id;
    private String nombre;
    private String descripcion;
    private Estudiante estudiante;
    private Date fechaDeCreacion;

    public Tarea(String nombre, String descripcion, Estudiante estudiante){
        this.id = UUID.randomUUID().toString();
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.estudiante = estudiante;
        this.fechaDeCreacion = Calendar.getInstance().getTime();
    }

    public String getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nuevoNombre){
        this.nombre = nuevoNombre;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Estudiante getEstudiante() {
        return estudiante;
    }

    public void setEstudiante(Estudiante estudiante) {
        this.estudiante = estudiante;
    }

    public Date getFechaDeCreacion() {
        return fechaDeCreacion;
    }

    public void setFechaDeCreacion(Date fechaDeCreacion) {
        this.fechaDeCreacion = fechaDeCreacion;
    }
    @Override
    public String toString() {
        return "Tarea{" +
                "id='" + id + '\'' +
                ", nombre='" + nombre + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", estudiante=" + estudiante +
                ", fechaDeCreacion=" + fechaDeCreacion +
                '}';
    }
}
