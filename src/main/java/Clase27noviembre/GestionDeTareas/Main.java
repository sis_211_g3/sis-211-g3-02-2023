package Clase27noviembre.GestionDeTareas;

import java.util.Calendar;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Data data = new Data();
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        for (int i = 0; i < n; i++) {
            System.out.println("Ingrese los datos para el estudiante");
            System.out.println("Nombre: ");
            String nombre = scanner.next();

            System.out.println("Apellido: ");
            String apellido = scanner.next();

            System.out.println("Edad: ");
            Integer edad = scanner.nextInt();

            Estudiante estudiante = new Estudiante(nombre, apellido, edad);
            System.out.println("Ingrese los datos para la tarea");

            System.out.println("Nombre: ");
            String nombreTarea = scanner.next();

            System.out.println("Descripcion: ");
            String descripcion = scanner.next();

            Tarea tarea = new Tarea(nombreTarea, descripcion, estudiante);
            data.addTarea(tarea);
        }
        System.out.println("Tareas registradas");
        for (Tarea tarea: data.getTareas()){
            System.out.println(tarea);
        }

    }
}
