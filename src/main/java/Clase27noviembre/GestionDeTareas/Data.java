package Clase27noviembre.GestionDeTareas;

import java.util.ArrayList;

public class Data {
    private ArrayList<Tarea> tareas;

    public Data(){
        this.tareas = new ArrayList<>();
    }

    public void addTarea(Tarea tarea){
        this.tareas.add(tarea);
    }

    public ArrayList<Tarea> getTareas(){
        return this.tareas;
    }
}
