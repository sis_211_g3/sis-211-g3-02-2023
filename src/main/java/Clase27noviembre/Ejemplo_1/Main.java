package Clase27noviembre.Ejemplo_1;

import java.util.Scanner;

public class Main {
    static Scanner scanner = new Scanner(System.in);
    static ArrayData data = new ArrayData();
    public static void main(String[] args) {

        int opcion;
        do{
            System.out.println("Menu");
            System.out.println("1.- Registrar libro\n2.- Ver libros\n3.- Buscar Libro" +
                    "\n4.- Editar Libro\n5.- Eliminar Libro\n0.- Salir");
            opcion = scanner.nextInt();
            manejadorOPciones(opcion);
        }while(opcion != 0);
    }
    public static void manejadorOPciones(int option){
        switch (option){
            case 1:{
                String titulo = scanner.next();
                int numeroPaginas = scanner.nextInt();
                String autor = scanner.next();
                Libro libro = new Libro(titulo, numeroPaginas, autor);
                data.aniairLibro(libro);
                break;
            }
            case 2:{
                System.out.println("ver libros");
                data.getData();
                break;
            }
            case 3:{
                System.out.println("Buscar");
                String id = scanner.next();
                System.out.println(data.buscarLibro(id));
                break;
            }
            case 4:{
                System.out.println("Editar");

                break;
            }
            case 5:{
                System.out.println("Eliminar");
                String id = scanner.next();
                data.eliminarLibro(id);
                break;
            }
            case 0:{
                System.out.println("programa terminado");
                break;
            }
            default:{
                System.out.println("no se tiene la opcion");
                break;
            }
        }
    }
}
