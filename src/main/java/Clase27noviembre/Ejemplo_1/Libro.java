package Clase27noviembre.Ejemplo_1;

import java.util.UUID;

public class Libro {
    private String id;
    private String titulo;
    private int numeroPaginas;
    private String autor;

    public Libro(String titulo, int numeroPaginas, String autor){
        this.id = UUID.randomUUID().toString();
        this.autor = autor;
        this.numeroPaginas = numeroPaginas;
        this.titulo = titulo;
    }


    public int getNumeroPaginas() {
        return this.numeroPaginas;
    }

    public String getAutor() {
        return this.autor;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setNumeroPaginas(int numeroPaginas) {
        this.numeroPaginas = numeroPaginas;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    @Override
    public String toString() {
        return "Libro{" +
                "id='" + id + '\'' +
                ", titulo='" + titulo + '\'' +
                ", numeroPaginas=" + numeroPaginas +
                ", autor='" + autor + '\'' +
                '}';
    }
}

