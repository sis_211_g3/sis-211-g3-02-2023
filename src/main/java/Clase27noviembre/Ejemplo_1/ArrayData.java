package Clase27noviembre.Ejemplo_1;

import java.util.ArrayList;
import java.util.List;

public class ArrayData {
    private List<Libro> libros;

    public ArrayData(){
        this.libros = new ArrayList<>();
        addData();
    }

    private void addData(){
        this.libros.add(new Libro("Libro 1", 20, "Autor 1"));
        this.libros.add(new Libro("Libro 2", 20, "Autor 2"));
        this.libros.add(new Libro("Libro 3", 20, "Autor 3"));
        this.libros.add(new Libro("Libro 4", 20, "Autor 4"));
        this.libros.add(new Libro("Libro 5", 20, "Autor 5"));
        this.libros.add(new Libro("Libro 6", 20, "Autor 6"));
        this.libros.add(new Libro("Libro 7", 20, "Autor 7"));
    }
    /*
    aniadir libro, : hecho
    eliminar libro
    buscar
    editar
    ver informacion :hecho
     */
    public void aniairLibro(Libro libro){
        this.libros.add(libro);
    }

    public void getData(){
        if (libros.isEmpty()){
            System.out.println("no se tiene libros");
        }else {
            for(Libro i : this.libros){
                System.out.println(i);
            }
        }

    }

    public Libro buscarLibro(String id){
        for(Libro libro : this.libros){
            if (libro.getId().equals(id)){
                return libro;
            }
        }
        return null;
    }

    public void eliminarLibro(String id){
        Libro libro = buscarLibro(id);
        if (libro != null){
            this.libros.remove(libro);
            System.out.println("Libro eliminado");
        }else {
            System.out.println("El libro no existe");
        }
    }

    public void editarLibro(String id, String titulo, int numeroPaginas, String autor){
        Libro libro = buscarLibro(id);
        if (libro != null){
            if (libro.getAutor() != autor)
                libro.setAutor(autor);
            if (libro.getTitulo() != titulo)
                libro.setTitulo(titulo);
            if (libro.getNumeroPaginas() != numeroPaginas) {
                libro.setNumeroPaginas(numeroPaginas);
            }
        }else {
            System.out.println("Libro no encontrado");
        }
    }

    public List<Libro> getLibros() {
        return libros;
    }

    public void setLibros(List<Libro> libros) {
        this.libros = libros;
    }
}
