package Clase27noviembre.Ejercicio3;

import java.lang.constant.Constable;
import java.util.ArrayList;
import java.util.List;

public class GestorContactos {
    private List<Contacto> contactos;

    public GestorContactos(){
        this.contactos = new ArrayList<>();
    }

    public void aniadirContacto(Contacto contacto){
        this.contactos.add(contacto);
    }

    public Contacto buscarContactoPorNombre(String nombre){
        for (Contacto contacto : this.contactos){
            if (contacto.getNombre().equals(nombre)){
                return contacto;
            }
        }
        return null;
    }

    public void eliminarConatctoPorNombre(String nombre){
        Contacto contacto = this.buscarContactoPorNombre(nombre);
        if (contacto != null){
            this.contactos.remove(contacto);
        }
    }

    public void mostrarContactos(){
        for(Contacto contacto : this.contactos){
            System.out.println(contacto);
        }
    }

    public void actualizarContacto(String nombre, String name, String apellido){
        Contacto contacto = this.buscarContactoPorNombre(nombre);
        if (contacto != null){
            contacto.setApellido(apellido);
            contacto.setNombre(nombre);
        }

    }
}
