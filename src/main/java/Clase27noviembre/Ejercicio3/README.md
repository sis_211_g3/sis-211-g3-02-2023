

---

**Enunciado: Gestión de Contactos**

Desarrolla un programa para gestionar una lista de contactos. Debes implementar las siguientes clases:

1. **Contacto**: Esta clase debe tener propiedades como `nombre`, `apellido`, `correoElectronico` y `numeroTelefono`. :check 

2. **GestorContactos**: Esta clase debe manejar un ArrayList de objetos `Contacto` y métodos para agregar un contacto, buscar un contacto por nombre, eliminar un contacto y mostrar todos los contactos.

El programa principal debe permitir al usuario interactuar con la lista de contactos mostrando un menú con opciones como agregar contacto, buscar contacto, eliminar contacto y mostrar todos los contactos. El usuario podrá realizar múltiples acciones hasta que decida salir del programa.

---

Este ejercicio te permite practicar la creación de clases, trabajar con ArrayList para almacenar objetos y realizar operaciones básicas de gestión de contactos en una aplicación simulada.