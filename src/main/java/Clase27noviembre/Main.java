package Clase27noviembre;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> numeros = new ArrayList<>();
        numeros.add(5);
        numeros.add(10);
        numeros.add(7);
        numeros.add(8);
        numeros.add(3);
        numeros.add(12);
        ArrayList<Integer> pares = new ArrayList<>();
        ArrayList<Integer> impares = new ArrayList<>();
        for (Integer numero: numeros) {
            if (numero %2 ==0){
                pares.add(numero);
            }else {
                impares.add(numero);
            }
        }
        System.out.println(pares);
        System.out.println(impares);
        /*for (int i = 0; i < 100; i++) {
            numeros.add(i);
        }*/
        /*System.out.println(numeros);
        for (int i = 0; i < numeros.size(); i++) {
            if (numeros.get(i) %2 != 0){
                numeros.remove(i);
            }
        }
        System.out.println(numeros);*/
        /* System.out.println(numeros.contains(500));
        System.out.println(numeros.indexOf(8));*/
    }
}
