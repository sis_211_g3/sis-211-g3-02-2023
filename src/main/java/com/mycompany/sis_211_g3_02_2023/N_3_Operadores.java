/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.sis_211_g3_02_2023;

/**
 *
 * @author PERSONAL
 */
public class N_3_Operadores {
    public static void main(String[] args) {
        // 2 tipos de opradores
        
        /**
         * @Title: Operadores Aritmeticos
         * @Descripcion: permiten realizar operaciones aritemeticas, son 7 operadores
         * 1.- suma (+)
         * 2.- Resta (-)
         * 3.- multiplicacion (*)
         * 4.- Divicion (/)
         * 5.- modulo (%)
         * 6.- Incremento unitario (++)
         * 7.- Decremento unitario (--)
         */
        
        System.out.println("Operadores Aritmeticos");
         
        System.out.println("la suma de " + 6 + " + " + 6 + " = " + (6+6));
        
        System.out.println("la resta de " + 6 + " - " + 3 + " = " + (6-3));
        
        System.out.println("la multiplicacion de " + 6 + " * " + 2 + " = " + (6*2));
        
        System.out.println("la división de " + 6 + " / " + 6 + " = " + (6/6));
        
        System.out.println("El modulo de " + 6 + " % " + 6 + " = " + (6%6));
        
        
        System.out.println("incremmento y decremento unitario");
        
        int numero = 10;
        
        System.out.println("numero inicial " + numero);
        
        numero++;// numero + 1
        
        System.out.println("numero con un incremento unitario " + numero);
        
        numero--;// numero - 1
        
        System.out.println("numero con un decremento unitario " + numero);
        
        System.out.println("=======================");
        
        
        
        /**
         * @Title: Operadores Relacionales
         * @Descripcion: nos permite realizar compraciones entre dos valores
         * 1.- mayor que (>)
         * 2.- menor que (<)
         * 3.- mayor o igual que (>=)
         * 4.- mneor o igual que (<=)
         * 5.- igual que (==)
         * 6.- diferente de (!=)
         */
        
        System.out.println("Operadores Relacionales");
        System.out.println("10 es mayor que 5 =>" + (10 != 15));
        if (10 > 15) {
            System.out.println("si 10 es mayor qu 15");
        }else{
            System.out.println("10 no es mayor que 15");
        }
        
        System.out.println("10 es menor que 5 =>" + (10 < 5));
        
        
    }
}
