/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package TareasSolucionario.Tarea_1;

/**
 *
 * @author PERSONAL
 */
public class Ejercicio_2 {

    public static void main(String[] args) {
        System.out.println(maximumNumberOfStringPairs(new String[]{"cd", "ac", "dc", "ca", "zz"}));
        System.out.println(maximumNumberOfStringPairs(new String[]{"ab", "ba", "cc"}));
    }

    public static int maximumNumberOfStringPairs(String[] words) {
        int counter = 0;
        for (int i = 0; i < words.length; i++) {
            for (int j = i + 1; j < words.length; j++) {
                if (words[i].equals(revertWord(words[j]))) {
                    counter++;
                    break;
                }
            }
        }
        return counter;
    }

    public static String revertWord(String word) {
        return new StringBuilder().append(word).reverse().toString();
    }
}
