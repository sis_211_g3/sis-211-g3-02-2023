/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package TareasSolucionario.Tarea_1;

/**
 *
 * @author PERSONAL
 */
public class Ejercicio_1 {
    public static void main(String[] args) {
        System.out.println(isAcronym(new String[]{"alice", "bob", "charlie"}, "abc"));
        System.out.println(isAcronym(new String[]{"an", "apple"}, "a"));
    }
    
    public static boolean isAcronym(String[] words, String s) {
        StringBuilder builder = new StringBuilder();
        for (String word : words) {
            builder.append(word.charAt(0));
        }
        return builder.toString().equals(s);
    }
}
