/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Clase_14_octubre.Ejemplo_1;

import java.util.Scanner;

/**
 *
 * @author MSI
 */
public class Main {
    public static void main(String[] args) {
        Point a = new Point(1, 2);
        Point b = new Point(-2, 5);
        
        Scanner x  = new Scanner(System.in);
        
        int n = x.nextInt();
        Point[] points = new Point[n];
        for (int i = 0; i < n; i++) {
            points[i] = new Point(x.nextInt(), x.nextInt());
        }
        
        Point res = Point.sumaPuntos(points);
        System.out.println(res.getData());
        /*Point resul = b.sumaPuntos(a, b);
        System.out.println(resul.getData());
        a.setX(2);*/
    }
}
